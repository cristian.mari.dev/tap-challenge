/**
 * @desc Entidad encargada de controlar las funcionalidades del juego.
 */
class Game {

    /**
     * @desc Indica si inicio el juego.
     */
    Initialized = false;

    /**
     * @desc Indica si se gano el juego
     */
    Winner = false;

    /**
     * @desc Indica la perdida del juego.
     */
    GameOver = false;

    /**
     * @desc Fecha de inicio del juego
     */
    Timestamp = null;

    /**
     * @desc Niveles del juego
     */
    Levels = {
        "easy": {
            "columns": 12,
            "rows": 12,
            "mines": 20
        },
        "medium": {
            "columns": 16,
            "rows": 16,
            "mines": 40
        },
        "expert":{
            "columns": 32,
            "rows": 16,
            "mines": 99
        },
        "god": {
            "columns": 100,
            "rows": 100,
            "mines": 999
        }
    };

    /**
     * @desc Preferencias del usuario.
     */
    Preference = {
        "typeSave": "indexedDB",
        "currentLevel": this.Levels.easy
    };

    /**
     * @desc Minas restantes
     */
    RemainingMines = this.Preference.currentLevel.mines;

    /**
     * @desc Cuadros del juego.
     */
    Squares = null;

    /**
     * @desc Constructor de la clase.
     * 
     * @param { String } levelSelected
     * @param { Array } oldGame
     * 
     * @return { Game }
     */
    constructor( levelSelected, oldGame = null ){

        // Verificamos si existe un juego
        if( oldGame !== null ){
            return this.import( oldGame );
        }

        // Indicamos el nivel seleccionado.
        this.Preference.currentLevel = this.Levels.hasOwnProperty( levelSelected ) 
                                        ? this.Levels[ levelSelected ] : this.Levels.easy;

        // Minas restantes
        this.RemainingMines = this.Preference.currentLevel.mines;

        // Fecha de inicio
        this.Timestamp = null;

        // Generamos los cuadros.
        this.generateSquares();

        // Generamos las minas del juego.
        this.generateMines();

        // Generamos los indicadores de minas de proximidad
        this.generateNumbersOfProximity();

        return this;

    }

    /**
     * @desc Generamos los cuadros del juego.
     * 
     * @return { Boolean } - Indica el resultado de la operación.
     */
    generateSquares(){

        try{

            // Preferencias del juego
            const { currentLevel = this.Levels.easy } = this.Preference;

            // Cantidades de columnas y filas
            const { columns, rows } = currentLevel;

            // Creamos las posiciones de los cuadros.
            let squares = new Array( columns * rows );

            // Asignamos las posiciones de los cuadros.
            this.Squares = squares.fill( null ).map( ( nulled , index ) => ({
                "value": null,
                "row": Math.floor( index / columns ) + 1,
                "column": ( index % columns ) + 1,
                "neighbors": [],
                "revealed": false,
                "pointing": ""
            }));

            return true;

        }catch( error ){

            // Logueamos el error.
            console.log( error );

            return false;

        }

    }

    /**
     * @desc Genera las minas dentro de los cuadros del juego.
     * 
     * @return { Boolean } - 
     */
    generateMines(){

        try{

            // Preferencias del jugador.
            const { currentLevel = this.Levels.easy } = this.Preference;

            // Minas del juego
            const { mines } = currentLevel; 

            // Indices utilizados
            let indexeds = 0;

            // Recorremos hasta obtener la cantidad de minas real total.
            while( indexeds !== mines ){

                // Creamos una posición random
                let randomPosition = Math.floor( Math.random() * ( this.Squares.length - 1 ) );

                // Insertamos la bomba en el cuadro.
                this.Squares[ randomPosition ].value = '💣';

                // Incrementamos las bombas plantadas.
                indexeds++;

            }

            return true;

        }catch( error ){

            // Logueamos el error
            console.log( error );

            return false;

        }

    }

    /**
     * @desc Generamos los numeros indicadores de minas de proximidad.
     * 
     * @return { Boolean }
     */
    generateNumbersOfProximity(){

        try{

            // Preferencias del juego
            const { currentLevel = this.Levels.easy } = this.Preference;

            // Preferencias del juego
            const { columns, rows } = currentLevel;

            // Alias de los cuadros
            const Squares = this.Squares ?? [];

            // Recorremos los cuadros.
            Squares.forEach( ( square, index ) => {

                // Alias de los datos del cuadro
                const { column, row } = square;

                // Verificamos si la primera columna de la grilla.
                if( column === 1 ){

                    // Primer cuadro de la grilla.
                    if( row === 1  ){

                        // Indicamos los cuadros vecinos 
                        this.Squares[ index ].neighbors.push( index + 1 );
                        this.Squares[ index ].neighbors.push( index + columns );
                        this.Squares[ index ].neighbors.push( index + columns + 1 );

                    // Ultimo cuadro de la primer columna
                    }else if( row === rows ){

                        // Indicamos los cuadros vecinos
                        this.Squares[ index ].neighbors.push( index + 1 );
                        this.Squares[ index ].neighbors.push( index - columns );
                        this.Squares[ index ].neighbors.push( index - columns + 1 );

                    // Cuadro intermedios de la primer columna.
                    }else{

                        // Indicamos los cuadros vecinos
                        this.Squares[ index ].neighbors.push( index + 1 );
                        this.Squares[ index ].neighbors.push( index + columns );
                        this.Squares[ index ].neighbors.push( index + columns + 1 );
                        this.Squares[ index ].neighbors.push( index - columns );
                        this.Squares[ index ].neighbors.push( index - columns + 1 );

                    }

                // Verificamos la ultima columna 
                }else if( column === columns ){

                    // Primer cuadro de la ultima columna.
                    if( row === 1  ){

                        // Indicamos los cuadros vecinos 
                        this.Squares[ index ].neighbors.push( index - 1 );
                        this.Squares[ index ].neighbors.push( index + columns );
                        this.Squares[ index ].neighbors.push( index + columns - 1 );

                    // Ultimo cuadro de la primer columna
                    }else if( row === rows ){

                        // Indicamos los cuadros vecinos
                        this.Squares[ index ].neighbors.push( index - 1 );
                        this.Squares[ index ].neighbors.push( index - columns );
                        this.Squares[ index ].neighbors.push( index - columns - 1 );

                    // Cuadro intermedios de la primer columna.
                    }else{

                        // Indicamos los cuadros vecinos
                        this.Squares[ index ].neighbors.push( index - 1 );
                        this.Squares[ index ].neighbors.push( index + columns );
                        this.Squares[ index ].neighbors.push( index + columns - 1 );
                        this.Squares[ index ].neighbors.push( index - columns );
                        this.Squares[ index ].neighbors.push( index - columns - 1 );

                    }

                // Verificamos los cuadros centrales
                }else{ 

                    // Verificamos si es de la primer fila
                    if( row === 1 ){

                        // Indicamos los cuadros vecinos
                        this.Squares[ index ].neighbors.push( index - 1 );
                        this.Squares[ index ].neighbors.push( index + 1 );
                        this.Squares[ index ].neighbors.push( index + columns - 1 );
                        this.Squares[ index ].neighbors.push( index + columns );
                        this.Squares[ index ].neighbors.push( index + columns + 1 );

                    // Verificamos si es el ultimo item.
                    }else if( row === rows ){

                        // Indicamos los cuadros vecinos
                        this.Squares[ index ].neighbors.push( index - 1 );
                        this.Squares[ index ].neighbors.push( index + 1 );
                        this.Squares[ index ].neighbors.push( index - columns - 1 );
                        this.Squares[ index ].neighbors.push( index - columns );
                        this.Squares[ index ].neighbors.push( index - columns + 1 );

                    }else{

                        // Indicamos los cuadros vecinos
                        this.Squares[ index ].neighbors.push( index - 1 );
                        this.Squares[ index ].neighbors.push( index + 1 );
                        this.Squares[ index ].neighbors.push( index + columns - 1 );
                        this.Squares[ index ].neighbors.push( index + columns );
                        this.Squares[ index ].neighbors.push( index + columns + 1 );
                        this.Squares[ index ].neighbors.push( index - columns - 1 );
                        this.Squares[ index ].neighbors.push( index - columns );
                        this.Squares[ index ].neighbors.push( index - columns + 1 );

                    }

                }

                // Verificamos que no sea una mina
                if( square.value != "💣" ){

                    // Verificamos la cantidad de minas de proximidad al cuadro.
                    let mines = square.neighbors.filter( neighbor => {
                        
                        return Squares[neighbor].value == "💣";

                     }).length;

                    // Verificamos la cantidad de minas.
                    if( mines > 0 ){

                        // Indicamos la cantidad de minas de proximidad.
                        square.value = mines;

                    }

                }


            });

            return true;

        }catch( error ){

            // Logueamos el error
            console.log( error );

            return false;

        }

    }

    /**
     * @desc Indicamos la bandera
     * 
     * @param { EventTarget } e
     * @param { Number } index
     * 
     * @return { Game }
     */
    setPointing( e, index ){

        try{

            e.preventDefault();

            // Preferencias
            const { currentLevel = this.Levels.easy } = this.Preference;

            // Puntero
            const { pointing } = this.Squares[ index ];

            // Verificamos si es una bandera
            this.Squares[ index ].pointing = pointing === "" 
                                            ? '🚩' 
                                            : pointing === '🚩' ? '❓' : "";

            // Verificamos el numero de bombas restantes
            this.RemainingMines = currentLevel.mines - this.Squares.filter( square => ( square.pointing ===  '🚩' ) ).length;

            return this;

        }catch( error ){

            // Logueamos el error
            console.log( error );

            return this;

        }

    }

    /**
     * @desc Indicamos la bandera
     * 
     * @param { Number } index
     * 
     * @return { Game }
     */
    revealBox( index ){

        try{

            // Verificamos si es el primer bloque a revelar
            if( !this.Initialized ){
                // Indicamos que se inicia el juego.
                this.Initialized = true;
                // Indica la fecha de comienzo
                this.Timestamp = Date.now();
            }

            // Cuadro
            let square = this.Squares[ index ];

            // Verificamos si esta revelado
            if( !square.revealed ){

                // Revelamos el cuadro
                if(  square.value == "💣" ){

                    // Revelamos la bomba
                    this.Squares[ index ].revealed = true;

                    // Indicamos el game over
                    this.GameOver = true; // :(

                    // Revelamos todas las bombas.
                    this.revealedMines();

                }else if( square.value === null ){

                    // Revelamos el cuadro
                    this.Squares[ index ].revealed = true;


                    // recorremos los cuadros vecinos
                    this.Squares[ index ].neighbors.forEach( neighbor => this.revealBox( neighbor ) );

                }else if( typeof square.value === "number"){

                    // Revelamos el cuadro
                    this.Squares[ index ].revealed = true;

                }

            }
            
            // Reiniciamos el puntero
            this.Squares[ index ].pointing = '';

            // Calculamos las minas reveladas restantes
            this.calculateRevealed( index);

            // Calculamos las minas restantes.
            this.calculateRemainingMines( index );

            return this;

        }catch( error ){

            // Logueamos el error
            console.log( error );

            return this;

        }

    }

    /**
     * @desc Revelamos las banderas restantes
     * 
     * @return { Boolean }
     */
    revealedFlags( ){

        try{

            // Verificamos el numero de bombas restantes
            this.Squares.forEach(( square, key ) => {
                // Cuadro
                if( square.value === "💣" ) {
                    this.Squares[ key ].pointing = "🚩";
                }
            });

            return this;

        }catch( error ){

            // Logueamos el error
            console.log( error );

            return this;

        }

    }
    
    /**
     * @desc Revelamos las banderas restantes
     * 
     * @return { Boolean }
     */
    revealedMines( ){

        try{


            // Verificamos el numero de bombas restantes
            this.Squares.forEach( ( square, key ) => {

                // Cuadro
                if( square.value === "💣" ) {
                    this.Squares[ key ].revealed = true;
                }

            });

            return this;

        }catch( error ){

            // Logueamos el error
            console.log( error );

            return this;

        }

    }
    
    /**
     * @desc Calculamos las minas restantes
     */
    calculateRevealed( ){

        // Preferencias
        const { currentLevel = this.Levels.easy } = this.Preference;

        // Verificamos el numero de bombas restantes
        const revealedTotal = currentLevel.mines + this.Squares.filter( currentSquare => ( currentSquare.revealed ) ).length;

        // Tamaño total de cuadros
        const totalSquares = currentLevel.columns * currentLevel.rows;

        // Indicamos el restante de minas.
        if( revealedTotal === totalSquares ){

            // Ganador
            this.Winner = true;

            // Revelamos las banderas.
            if( !this.GameOver ){

                return this.revealedFlags();

            }

        }

        return this;

    }

    /**
     * @desc Calculamos las minas restantes
     */
    calculateRemainingMines( ){

        // Preferencias
        let { currentLevel = this.Levels.easy } = this.Preference;

        // Verificamos el numero de bombas restantes
        let RemainingMines = currentLevel.mines - this.Squares.filter( currentSquare => ( currentSquare.pointing ===  '🚩' )).length;

        // Indicamos el restante de minas.
        this.RemainingMines = RemainingMines;

    }

    /**
     * @desc Obtenemos el tiempo transcurrido del juego
     */
    getTimeElapsed( ){

        // Verificamos si se inicio el juego
        if( this.Initialized && !this.GameOver && !this.Winner ){

            // Tiempo transcurrido
            const elapsed = Math.ceil(( Date.now() / 1000 ) - ( this.Timestamp / 1000 ) );

            // Verificamos la cantidad de 0 iniciales.
            const ceros = {
                1: "00" + elapsed,
                2: "0" + elapsed,
                3: elapsed
            };

            return elapsed
                    ? ceros[ elapsed.toString().length ]
                    : "000";

        }else{

            return "000";

        }
        
    }

    /**
     * @desc Importamos los datos del juego.
     * 
     * @param { Object } gameSaved
     * 
     * @return { Boolean }
     */
    import( gameSaved ){

        // Indicamos loostados
        this.Initialized    = gameSaved.Initialized;
        this.Winner         = gameSaved.Winner;
        this.GameOver       = gameSaved.GameOver;
        this.RemainingMines = gameSaved.RemainingMines;
        this.Squares        = gameSaved.Squares;
        this.Timestamp      = Date.now() - Math.ceil( gameSaved.TimeElapsed * 1000 );
        this.TimeElapsed    = gameSaved.TimeElapsed;

        // Indicamos el nivel 
        this.Preference.currentLevel = gameSaved.CurrentLevel

        return this;

    }

    /**
     * @desc Exporta los datos del juego
     * 
     * @return { Boolean }
     */
    export(){

        return {
            "Initialized"       : this.Initialized,
            "Winner"            : this.Winner,
            "GameOver"          : this.GameOver,
            "RemainingMines"    : this.RemainingMines,
            "TimeElapsed"       : this.getTimeElapsed(),
            "Squares"           : this.Squares,
            "CurrentLevel"      : this.Preference.currentLevel,
            "Timestamp"         : this.Timestamp
        };

    }

}

export default Game;