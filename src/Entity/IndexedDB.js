/**
 * @desc Base de datos del navegador.
 */
class Entity_IndexedDB {

    /**
     * @desc Constructor
     * 
     * @return { void }
     */
    constructor(){

        // Instance
        this.db = new IndexedDB( "minesweeper", 1 );

    }


    /**
     * @desc Obtiene un juego guardado.
     * 
     * @return { Promise<Boolean> }
     */
    async getAllSaved(){

        // Obtenemos el ultimo juego
        return await this.db.getAll( "games" );

    }

    /**
     * @desc Obtiene un juego guardado.
     * 
     * @return { Promise<Boolean> }
     */
    async getLastGameSaved(){

        // Obtenemos el ultimo juego
        return await this.db.getAll( "games" )[0];

    }

    /**
     * @desc Guardamos un juego
     * 
     * @param { Object } data
     * 
     * @return { Promise<Boolean> }
     */
    async save( data ){

        try{

            // Guardamos un juego
            return await this.db.add( "games", data );

        }catch( error ){

            // Logueamos el error
            console.log( error );

            return false;

        }

    }

}

export default Entity_IndexedDB;