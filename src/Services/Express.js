/**
 * @desc Base de datos del navegador.
 */
class Entity_Express {

    /**
     * @desc Constructor
     * 
     * @return { void }
     */
    constructor(){
    }

    /**
     * @desc Obtiene un juego guardado.
     * 
     * @param { Function } callback
     * 
     * @return { Promise<Boolean> }
     */
    async getRecordsGame( callback = () => {} ){

        // Obtenemos el ultimo juego
        callback(await (await fetch("/games")).json());

    }

    /**
     * @desc Obtiene un juego guardado.
     * 
     * @param { Function } callback
     * 
     * @return { Promise<Boolean> }
     */
    async getLastGameSaved(callback = () => {}){

        // Obtenemos el ultimo juego
        callback(await (await fetch("/games/last")).json());

    }

    /**
     * @desc Guardamos un juego
     * 
     * @param { Object } data
     * @param { Function } callback
     * 
     * @return { Promise<Boolean> }
     */
    async add( body, callback = () => {} ){

        try{

            // Obtenemos el ultimo juego
            callback(await (await fetch("/games/save", { body, method: "POST" })));

        }catch( error ){

            // Logueamos el error
            console.log( error );

            return false;

        }

    }

}

export default Entity_Express;