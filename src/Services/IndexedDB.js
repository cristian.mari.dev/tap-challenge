/**
 * @desc Dependencias
 */
import IndexedDB from "../Utils/indexdDB";

/**
 * @desc Base de datos del navegador.
 */
class Entity_IndexedDB {

    /**
     * @desc Constructor
     * 
     * @return { void }
     */
    constructor(){

        // Instance
        this.db = new IndexedDB( "minesweeper", 1 );

    }

    /**
     * @desc Obtiene un juego guardado.
     * 
     * @param { Function } callback
     * 
     * @return { Promise<Boolean> }
     */
    async getRecordsGame(callback = () => {}){

        // Obtenemos el ultimo juego
        callback( await this.db.getAll( "games" ) );

    }

    /**
     * @desc Obtiene un juego guardado.
     * 
     * @param { Function } callback
     * 
     * @return { Promise<Boolean> }
     */
    async getLastGameSaved(callback = () => {}){

        // Obtenemos el ultimo juego
        callback( await this.db.getAll( "games" )[0] );

    }

    /**
     * @desc Guardamos un juego
     * 
     * @param { Object } data
     * @param { Function } callback
     * 
     * @return { Promise<Boolean> }
     */
    async add( data, callback ){

        try{

            // Guardamos un juego
            callback(await this.db.add( "games", data ));

        }catch( error ){

            // Logueamos el error
            console.log( error );

            return false;

        }

    }

}

export default Entity_IndexedDB;