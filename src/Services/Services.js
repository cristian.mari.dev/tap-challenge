/**
 * @desc Dependencias
 */
import ServiceExpress from "./Express";
import ServiceIndexedDB from "./IndexedDB";

/**
 * @desc Base de datos del navegador.
 */
class Services { 

    /**
     * @desc Servicios disponibles
     */
    Availables = {
        "IndexedDB": new ServiceIndexedDB(),
        "Express": new ServiceExpress()
    };

    /**
     * @desc Constructor
     * 
     * @param { String } storageType - 
     * 
     * @return { void }
     */
    constructor( storageType = "IndexedDB" ){
        return this.Availables[storageType];
    }

}

export default Services;