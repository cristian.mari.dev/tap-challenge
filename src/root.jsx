/**
 * @desc Dependencias
 */
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

/**
 * @desc Rutas.
 */
import App from "./View/Home";

/**
 * @desc Estilos
 */
import GlobalStyles from "./fonts";

document.oncontextmenu = e => e.preventDefault()

/**
 * @desc Renderiza la aplicación.
 */
ReactDOM.render(
    <BrowserRouter>
        <GlobalStyles/>
        <App history={ History } />
        <svg>
            <filter id="turbulence" x="0%" y="0" width="100%" height="100%">

                <feTurbulence id="sea-filter" numOctaves="3" seed="2" baseFrequency="0.02 0.09" />
                <feDisplacementMap scale="4" in="SourceGraphic" />
                
                <animate xlinkHref="#sea-filter" 
                         attributeName="baseFrequency" 
                         dur="120s" 
                         keyTimes="0;0.25;1" 
                         values="0.02 0.0; 0.04 0.04; 0.02 0.06" 
                         repeatCount="indefinite" 
                />

            </filter>
        </svg>
    </BrowserRouter>, 
document.getElementById('root'));
