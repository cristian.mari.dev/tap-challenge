/**
 *  @desc Dependencias
 */
import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    @font-face {
        font-family: 'Calculator';
        src: url("./assets/fonts/Calculator.eot");
        src: local('☺'), url('./assets/fonts/Calculator.woff') format('woff'), 
                         url('./assets/fonts/Calculator.ttf') format('truetype'), 
                         url('./assets/fonts/Calculator.svg') format('svg');
        font-weight: normal;
        font-style: normal;
    }
`;
