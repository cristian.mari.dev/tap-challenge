/**
 * @desc Base de datos.
 */
class IndexedDB {

    /**
     * @desc Constructor.
     */
    constructor ( name, version ) {

        // Instance
        this.IndexedDB = window.indexedDB || window.mozIndexdedDB || window.webkitIndexedDB || window.msIndexedDB;

        // Opciones de configuración.
        this.options = {
            name, version
        }

    }

    /**
     * @desc Verificamos si existe la base.
     * 
     * @param { String } name
     * 
     * @return { Promise<Boolean> }
     */
    static verifyDatabase( name, callback ){ 

        // Instance
        let IDB = window.indexedDB || window.mozIndexdedDB || window.webkitIndexedDB || window.msIndexedDB;

        // Verificamos el acceso.
        let accessDatabases = IDB.webkitGetDatabaseNames();

        // Obtenemos las bases de datos.
        accessDatabases.onsuccess = ( e ) => {

            try{

                // Indica si existe.
                let exist = false;

                // Bases de datos.
                let databases = [...e.target.result];

                // Recorremos los resultados.
                if( Array.isArray(databases) && databases.length > 0 ){
                    databases.forEach( databaseName => {
                        
                        if( databaseName === name) {
                            exist = true;
                        }

                    });
                }

                callback( exist );

            }catch( error ){

                // Logueamos el error
                console.log(error);

                callback( false );

                return false;

            }

        };

        // Rechazamos la promesa
        accessDatabases.onerror = (e) => {

            // Logueamos el error
            console.log(e);

            resolve(false);

        }

    }

    /**
     * @desc Conexion a la base de datos.
     * 
     * @param { Function } success 
     * @param { Function } reject 
     * 
     * @return { Boolean }
     */
    connect( success = () => {}, reject = () => {} ){

        try{

            // Abrimos la base de datos o la creamos
            this.db = this.IndexedDB.open( this.options.name, this.options.version );

            // Si se creo la db correctamente.
            this.db.onupgradeneeded = e => this.upgraded(e);          
            this.db.onsuccess = success;
            this.db.onerror = reject;          

            return true;

        }catch( error ){

            // Logueamos el error
            console.log( error );

            return false;

        }

    }

        /**
     * @desc Conexion a la base de datos.
     * 
     * @param { Function } success 
     * @param { Function } reject 
     * 
     * @return { Boolean }
     */
    create( success = () => {}, reject = () => {} ){

        try{

            // Abrimos la base de datos o la creamos
            this.db = this.IndexedDB.open( this.options.name, this.options.version );

            // Si se creo la db correctamente.
            this.db.onupgradeneeded = e => this.upgraded(e);          
            this.db.onsuccess = success;
            this.db.onerror = reject;          

            return true;

        }catch( error ){

            // Logueamos el error
            console.log( error );

            return false;

        }

    }

    /**
     * @desc Si se creo la bd, creamos tablas.
     * 
     * @return { Boolean }
     */
    upgraded(){

        try{

            // DatabaseActive
            let active = this.db.result;

            // Tables.
            let tables = {
                "games": {
                    "indexes" : [['by_id','id',{ unique: false }]],
                    "options": {
                        "keyPath": "id",
                        "autoIncrement": true
                    }
                }
            };

            // Crea un almacen de datos.
            for(let key in tables){

                // Tabla creada.
                let table = active.createObjectStore(key, {...tables[key].options});

                // Creamos los indices.
                for(let indice of tables[key].indexes){
                    table.createIndex(indice[0], indice[1], indice[2]);
                }

            }

            return true;

        }catch( error ){

            // Logueamos el error.
            console.log( error );

            return false;

        }


    }

    /**
     * @desc Reconectamos la base de datos.
     * 
     * @param { Function }  callback
     * 
     * @return { Boolean }
     */
    async reconectWhenNeed( callback = () => {} ){

        try{

            // Conectamos la base de datos.
            if( this.db === undefined ) {

                // Conectamos la base de datos.
                await this.connect(async function( e ){

                    try{

                        callback( this.db.result );
                        
                        return true;
                        
                    }catch( error ){

                        // Logueamos el errorc
                        console.log( error );

                        return false;

                    }

                }.bind(this));

            }else{

                callback( this.db.result );
                
            }

            return true;

        }catch( error ){

            // Logueamos el error
            console.log( error );

            return false;

        }

    }

    /**
     * @desc Agrega un item a un almacen.
     * 
     * @param { String } key 
     * @param { Object } data
     * 
     * @return { Promise }
     */
    async add( key, data ){

        return new Promise( async function( resolve, reject ){

            try{

                this.reconectWhenNeed(function( active ){

                    // Transaction
                    let transaction = active.transaction([key], "readwrite");

                    // Table
                    let table = transaction.objectStore(key);

                    // Actualizamos el almacen.
                    let request = table.add(data);

                    request.onerror = reject;
                    request.onsuccess = () => resolve();

                });

                return true;

            }catch( error ){

                // Logueamos el error
                console.log( error );

                return false;

            }

        }.bind(this));

    }

    /**
     * @desc Actualiza un item a un almacen.
     * 
     * @param { String } key 
     * @param { Object } data
     * 
     * @return { Promise }
     */
    async put( key, data ){

        return new Promise( async function( resolve, reject ){

            try{

                this.reconectWhenNeed(function( active ){

                    // Transaction
                    let transaction = active.transaction([key], "readwrite");

                    // Table
                    let table = transaction.objectStore(key);

                    // Actualizamos el almacen.
                    let request = table.put(data);

                    request.onerror = reject;
                    request.onsuccess = () => resolve();

                });

                return true;

            }catch( error ){

                // Logueamos el error
                console.log( error );

                return false;

            }

        }.bind(this));

    }

    /**
     * @desc Obtiene todos los registros de un almacen de indexedDB.
     * 
     * @param { string } key
     * 
     * @return { Promise }
     */
    getAll( key ) {

        return new Promise( ( resolve, reject ) => {

            // Reconecta si necesita con la base de datos.
            this.reconectWhenNeed(function( active ){

                // Transaction
                let transaction = active.transaction([key], "readwrite");

                // Table
                let table = transaction.objectStore(key);

                // Respuesta.
                let response = [];

                // Actualizamos el almacen.
                table.openCursor().onsuccess = e => {

                    // Resultado de la busqueda.
                    let { result } = e.target;

                    // Si no hubo respuesta retornamos null.
                    if( result === null )
                        return result;

                    // Añadimos los resultados encontrados a la respuesta.
                    response.push( result.value );
                    result.continue();

                };

                // Añadimos los metodos de respuesta.
                table.onerror = () => reject();
                transaction.oncomplete = () => resolve(response);

            })

        });

    }
    

    /**
     * @desc Buscamos un elemento por su valor.
     * 
     * @param { String } key
     * @param { String } searchValue
     * @param { String } index
     * 
     * @return { Boolean }
     */
    getByKeyValue( key, searchKey, searchValue ) {

        
        return new Promise( ( resolve, reject ) => {

            try{

                // Conectamos a la base
                this.reconectWhenNeed(function( active ){

                    // Transaction
                    let transaction = active.transaction([key], "readonly");

                    // Table
                    let table = transaction.objectStore( key );

                    let indice = table.index( searchKey );

                    // Rango de llave.
                    var singleKeyRange = IDBKeyRange.only( searchValue );

                    // Cuando encontro con exito.
                    let cursor = indice.openCursor( singleKeyRange );

                    // Resultado
                    let result = [];

                    // Datos obtenidos correctamente.
                    cursor.onsuccess = e => {


                        // Continuamos con los resultados
                        if( e.currentTarget.result !== null ){
                            
                            // Añadimos el resultado
                            result.push( e.currentTarget.result.value );
                            
                            // Movemos el puntero
                            e.currentTarget.result.continue();

                        }else{
                            // Continuamos.
                            resolve( result )
                        }

                    };

                    // Añadimos los metodos de respuesta.
                    cursor.onerror = reject;

                });

            }catch( error ){

                // Logueamos el error.
                console.log( error );

                return false;

            }

        });

    }


    
    /**
     * @desc Buscamos un elemento por su valor.
     * 
     * @param { String } key
     * @param { String } searchValue
     * @param { String } index
     * 
     * @return { Boolean }
     */
    getByKey( key, searchValue, index) {

        
        return new Promise( ( resolve, reject ) => {

            try{

                // Conectamos a la base
                this.reconectWhenNeed(function( active ){

                    // Transaction
                    let transaction = active.transaction([key], "readonly");

                    // Table
                    let table = transaction.objectStore(key);

                    // Rango de llave.
                    var singleKeyRange = IDBKeyRange.only(searchValue);

                    // Cuando encontro con exito.
                    let cursor = table.openCursor( singleKeyRange );

                    // Datos obtenidos correctamente.
                    cursor.onsuccess = e => {

                        // Alias
                        let { result } = e.currentTarget;

                        // Continuamos.
                        resolve( result )

                    };

                    // Añadimos los metodos de respuesta.
                    cursor.onerror = reject;

                });

            }catch( error ){

                // Logueamos el error.
                console.log( error );

                return false;

            }

        });

    }

    /**
     * @desc Eliminamos un item del almacen.
     * 
     * @param { String } key
     * @param { String } byValue
     * 
     * @return { Boolean}
     */
    delete( key, byValue ) {

        return new Promise( ( resolve, reject ) => {

            try{

                // Conectamos a la base
                this.reconectWhenNeed(function( active ){

                    // Transaction
                    let transaction = active.transaction([key], "readwrite");

                    // Añadimos los metodos de respuesta.
                    transaction.onerror = reject;

                    // Table
                    let table = transaction.objectStore(key);

                    // Actualizamos el almacen.
                    let request = table.delete( byValue.id );

                    // Ejecutamos la resolución
                    request.onsuccess = resolve;            
                    
                });

            }catch( error ){

                // Logueamos el error.
                console.log( error );

                return false;

            }

        });

    }
    
    /**
     * @desc Eliminamos un item del almacen.
     * 
     * @param { String } key
     * @param { String } byValue
     * 
     * @return { Boolean}
     */
    deleteAll( key ) {

        
        return new Promise( ( resolve, reject ) => {

            // Active DB
            let active = this.db.result;

            // Transaction
            let transaction = active.transaction([key], "readwrite");

            // Añadimos los metodos de respuesta.
            transaction.onerror = reject;

            // Table
            let table = transaction.objectStore(key);

            // Actualizamos el almacen.
            let request = table.clear();

            // Ejecutamos la resolución
            request.onsuccess = (e) => {
                resolve(e);            
            }
            request.onerror = (e) => {
                reject(e);
            }
            
        });

    }

};

export default IndexedDB;