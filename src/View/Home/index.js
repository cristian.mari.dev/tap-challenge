/**
 * @desc Dependencias
 */
import React, { useState, useEffect } from "react";

/**
 * @desc Entidades
 */
import Game from "../../Entity/Game";
import Service from "../../Services/Services";


/**
 * @desc Secciones
 */
import Configuration from "./Sections/Configuration";
import Levels   from "./Sections/Levels";
import Counters from "./Sections/Counters";
import Board    from "./Sections/Board";
import Memory   from "./Sections/Memory";

/**
 * @desc Estilos
 */
import { Root, GameBoard, Mask, SaveGame } from "./styles";

/**
 * @desc Vista
 */
const Home = () => {

    // Nivel actual
    const [ typeStorage, setTypeStorage ] = useState( "IndexedDB" );
    // Juego
    const [ game, setGame ] = useState( new Game() );
    // Preferencias
    const [ level, setLevel ] = useState( "easy" );
    // Estados del juego
    const [ updateGame, setUpdateGame ] = useState( null );
    const [ saveGame, setSaveGame ] = useState( false );
    // Partidas guardadas
    const [ gamesPlayed, setGamesPlayed ] = useState( null );
    const [ selectOldGame, setSelectOldGame ] = useState( false );

    // Preferencias de juego
    const { Preference, Squares, RemainingMines } = game;
    
    // Nivel actual.
    const { currentLevel } = Preference;

    /**
     * @desc Inicial
     */
    useEffect( () => {

        // Servicio de almacenamiento
        let service = new Service( typeStorage );

        // Obtenemos todos las partidas jugadas
        service.getRecordsGame( setGamesPlayed );

    },[]);

    /**
     * @desc Detecta los cambios de la dificultad
     */
    useEffect(() => {
        // Indicamos el nuevo nivel.
        setGame( new Game( level ) );
    }, [ level ])

    /**
     * @desc Detectamos la selección de banderas
     */
    const setPointing = ( e, index ) => {
        // Indicamos la nueva bandera
        let newGame = game.setPointing( e, index );
        // Actualizamos el juego
        setUpdateGame( newGame );
    };
    
    /**
     * @desc Detectamos la selección de banderas
     */
    const handleRevealed = index => {
        // Indicamos la nueva bandera
        let newGame = game.revealBox( index );
        // Actualizamos el juego
        setUpdateGame( newGame );
    };

    /**
     * @desc Guardamos el juego
     */
    const handleSaveGame = async data =>{

        /**
         * @desc Servicios de almacenamiento.
         */
        let service = new Service( typeStorage );

        /**
         * @desc Guardamos el juego
         */
        await service.add( data );

        /**
         * @desc Indicamos el guardado
         */
        setSaveGame( true );

    };

    /**
     * @desc Deteetamos cuando se debe actualizar el juego
     */
    useEffect(() => {

        // Actualizamos el juego
        if( updateGame !== null ){
            setGame( updateGame );
            setUpdateGame( null );
        }

    })

    /**
     * @desc Detectamos cuando se guarda un juego
     */
    useEffect(() => {

        // Verificamos si se guardo uno
        if( saveGame === true ){

            // Actualizamos la lista de juegos guardados
            let service = new Service( typeStorage );

            // Obtenemos los juegos
            service.getRecordsGame( setGamesPlayed );

            // Indicamos que el cese de actualización basada en el guardado.
            setSaveGame( false );
        }

    }, [saveGame]);

    /**
     * @desc Detectamos las partidas importadas
     */
    useEffect(() => {

        // Verificamos si se indico un juego
        if( selectOldGame ){
            // Importamos la partida juego.
            let newGame = game.import( selectOldGame );
            // Desactivamos la importación
            setSelectOldGame( false );
            // Actualizamos el juego
            setUpdateGame( newGame );
        }

    }, [selectOldGame]);

    return (

            <Root>

                { /* Formato de guardado */ }
                <Configuration type={ typeStorage } onChange={ newTypeStorage => setTypeStorage( newTypeStorage ) } />

                { /* Juego */ }
                <GameBoard >

                    { /* Niveles del juego */ }
                    <Levels preference={ game.Preference } data={ game.Levels } onChange={ newLevel => setLevel( newLevel ) } />

                    { /* Contadores */ }
                    <Counters   onReset={ () => setUpdateGame( new Game( level ) ) }
                                game={ game }
                                gameOver={ game.GameOver } 
                                winner={ game.Winner }
                                getDuration={ () => game.getTimeElapsed() }
                                remainingMines={ ( RemainingMines < 10 ? "00" : "0" ) + ( RemainingMines > 0 ? RemainingMines : 0 ) } 
                    />

                    { /* Tablero del juego */ }
                    <Board  gameOver={ game.GameOver }
                            rows={ currentLevel.rows }
                            columns={ currentLevel.columns }
                            data={ Squares } 
                            onSetPoiting={ ( e, index ) => setPointing( e, index ) }
                            onRevealead={ index  => handleRevealed( index ) }
                    />

                    <SaveGame onClick={ () => handleSaveGame( game.export() ) }>Guardar Estado del juego</SaveGame>

                </GameBoard>

                <Mask />
                
                { /* Juegos guardados */ }
                <Memory data={ gamesPlayed } onSelect={ oldGame => setSelectOldGame( oldGame ) } />


            </Root>
    );

};

export default Home;