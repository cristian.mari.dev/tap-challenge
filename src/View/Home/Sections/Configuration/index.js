/**
 * @desc Dependencias
 */
import React, { useState } from "react";

/**
 * @desc Estilos
 */
import { Root, Technology, Label, Input } from "./styles";

/**
 * @desc Componente de niveles
 */
export default ({ ...props }) => {


    /*  Intencionalmente estoy dejando las tecnologias hardcode, 
        para no switchear con el servicio de node 
    */

    return (
        <Root>
            <Technology onClick={ () => props.onChange( "IndexedDB" ) } active={ props.type === "IndexedDB" }>
                <Input checked={ props.type === "IndexedDB" } type="radio" name="technology" value="IndexedDB" disabled />
                <Label>IndexedDB</Label>
            </Technology>
            <Technology onClick={ () => props.onChange( "IndexedDB" ) } active={ props.type === "node.js" }>
                <Input checked={ props.type === "node.js" } type="radio" name="technology" value="node.js" disabled />
                <Label>Node.js</Label>
            </Technology>
        </Root>
    );

};