/**
 *  @desc Dependencias
 */
import styled, { css } from "styled-components";

/**
 * @desc Raiz
 */
export const Root = styled.div`
    display:flex;
    background:#070707;
    width:100%;
    height:60px;
    max-width:380px;
    flex-wrap:nowrap;
    flex-direction:row;
    align-items:center;
    justify-content: space-around;
    z-index:9;
    margin:2.5% auto;
    margin-bottom:0%;
    padding:5px;
    border:2px solid #070707;
`;

/**
 * @desc Contador
 */
export const Technology = styled.div`
    display:flex;
    background:#c33;
    ${
        props => props.active &&
            css`
                color:white;
            `
    }
    width:calc(50% - 20px);
    height:40px;
    flex-wrap:nowrap;
    flex-direction:row;
    align-items:center;
    justify-content:center;
    padding:5px;
    cursor:pointer;
`;

/**
 * @desc Estado de la aplicación
 */
export const Input = styled.input`
    background:transparent;
    border-radius:50%;
    border:3px solid #111;
    padding:5px;
    appearance:none;
    margin-bottom:2px;
    outline:0px;
    cursor:pointer;

    &:checked{
        background:white;
        border-color:white;
    }
`;

/**
 * @desc Estado de la aplicación
 */
export const Label = styled.h6`
    font-size:18px;
    text-align:center;
    padding:5px 0px;
`;
