/**
 * @desc Dependencias
 */
import React from "react";

/**
 * @desc Estilos
 */
import { Root, Label, Levels, Level } from "./styles";


// @ToDo: Traductor.
const Translate = {
    "easy": "Facil",
    "medium": "Medio",
    "expert": "Experto",
    "god": "Dios"
};

/**
 * @desc Componente de niveles
 */
export default ({ ...props }) => {

    // Alias de las propiedades
    const { 

        // Nivel actual.
        preference = {},

        // Niveles posibles
        data, 
        
        // Cambio de niveles
        onChange = () => {} 

    } = props;

    return (
        <Root>

            { /* Niveles */ }
            <Label>Niveles</Label>

            { /* Numero de niveles */ }
            <Levels>
                {
                    Object.entries( props.data ).map( ([ difficulty, level ], index ) => (
                        <Level active={ preference.currentLevel.mines === level.mines } key={ index } onClick={ () => onChange( difficulty ) }>{ Translate[ difficulty ] }</Level>
                    ))
                }
            </Levels>

        </Root>
    );

};