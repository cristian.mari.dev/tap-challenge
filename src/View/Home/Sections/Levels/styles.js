/**
 *  @desc Dependencias
 */
import styled, { css } from "styled-components";

/**
 * @desc Visor
 */
const Visor = css`
    border-top-color:#878787;
    border-left-color:#878787;
    border-right-color:#fff;
    border-bottom-color:#fff;
    border-width:1px;
    border-style:solid;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
`;

/**
 * @desc Raiz
 */
export const Root = styled.div`
    display:flex;
    flex-wrap:nowrap;
    align-items:center;
    justify-content:space-around;
    margin-bottom:10px;
    ${ Visor }
    padding:10px;
`;

/**
 * @desc Etiqueta
 */
export const Label = styled.h1`
    margin:0px;
    font-family:Roboto;
    font-size:14px;
`;

/**
 * @desc Niveles
 */
export const Levels = styled.div`
    display:flex;
    flex-wrap:nowrap;
`;

/**
 * @desc Nivel
 */
export const Level = styled.p`
    background:#ddd;
    color:#222;
    margin:0px 5px;
    padding:5px 10px;
    ${Visor}
    ${
        props => props.active &&
        css`
            background:#ccc;
        `
    }
`;