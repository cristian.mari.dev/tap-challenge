/**
 * @desc Dependencias
 */
import React from "react";

/**
 * @desc Estilos
 */
import { Root, Square } from "./styles";

/**
 * @desc Colores
 */
const COLORS = {
    1: "green",
    2: "salmon",
    3: "firebrick",
    4: "darkred",
    5: "red",
    "?": "black"
};

/**
 * @desc Componente de niveles
 */
export default React.memo(({ ...props }) => {


    return (
        <Root {...props }>

            { /* Cuadro */ }
            {
                props.data.map(
                    ( square, index ) => ( 
                        <Square key={ index }
                                revealed={ square.revealed }
                                color={ !square.revealed ? COLORS[ "?" ] : COLORS[ square.value] } 
                                onContextMenu={ e => !props.gameOver && !square.revealed && props.onSetPoiting( e, index ) }
                                onClick={ () => !props.gameOver && props.onRevealead( index ) }>
                            {
                                square.revealed 
                                    ? square.value 
                                    : square.pointing 
                            }
                        </Square> 
                    ) 
                )
            }
            
        </Root>
    );

});