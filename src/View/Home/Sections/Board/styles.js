/**
 *  @desc Dependencias
 */
import styled, { css } from "styled-components";

/**
 * @desc Raiz
 */
export const Root = styled.div`
    display:grid;

    ${
        props => props.rows &&
            css`
                grid-template-rows: repeat( ${ props.rows }, 1fr ) ;
            `
    }
    ${
        props => props.columns &&
            css`
                grid-template-columns: repeat( ${ props.columns }, 1fr ) ;
            `
    }
    background:#ccc;
    flex-wrap:wrap;
    margin:10px auto;
    border-radius:5px;
`;

/**
 * @desc Contador
 */
export const Square = styled.div`
    display:flex;
    width:12px;
    height:12px;
    border-width:1px;
    border-style:solid;
    border-top-color:#fff;
    border-right-color:#fff;
    border-left-color:#818181;
    border-bottom-color:#818181;
    justify-content:center;
    align-items:center;
    font-weight:bold;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    
    ${
        props => props.revealed && 
        css `
            background:#dfdfdf;
            border-left-color:#818181;
            border-bottom-color:#818181;
        `
    }
    ${
        props => props.color && 
        css `
            color: ${ props.color === "orange" ? "#f88" : props.color };
        `
    }
    font-size:10px;
    cursor:pointer;
    padding:2px;
`;