/**
 * @desc Dependencias
 */
import React, { useState } from "react";

/**
 * @desc Estilos
 */
import { Root, Memory, Title, Saved, DaySaved, Output } from "./styles";

/**
 * @desc Iconos
 */
import SelectIcon from '@material-ui/icons/Adjust';

/**
 * @desc Componente de niveles
 */
export default ({ ...props }) => {

    return (
        <Root>

            <Title>Juegos Guardados</Title>
            
            <Memory>

                {
                    Array.isArray( props.data ) && props.data.length > 0 && props.data.map( game => {

                        return (
                            <Saved onClick={ () => props.onSelect( game ) }>
                                <SelectIcon/>
                                <DaySaved>{ new Date( game.Timestamp ).toLocaleString() }</DaySaved>
                            </Saved>
                        );

                    })
                }

                {
                    ( !Array.isArray( props.data ) || props.data.length === 0 ) &&
                            <Output>No tiene partidas guardadas.</Output>
                }

            </Memory>

        </Root>
    );

};