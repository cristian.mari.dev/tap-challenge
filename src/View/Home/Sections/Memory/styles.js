/**
 *  @desc Dependencias
 */
import styled, { css } from "styled-components";

/**
 * @desc Visor
 */
const Visor = css`
    border-top-color:#878787;
    border-left-color:#878787;
    border-right-color:#fff;
    border-bottom-color:#fff;
    border-width:1px;
    border-style:solid;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
`;

/**
 * @desc Raiz
 */
export const Root = styled.div`
    display:flex;
    background:#ddd;
    width:100%;
    max-width:380px;
    z-index:9;
    margin:0% auto;
    margin-bottom:0%;
    padding:10px;
    flex-wrap:wrap;
    box-shadow: 2px 2px 2px -1px rgba(0,0,0,0.3)
`;

/**
 * @desc Listado de juegos guardados
 */
export const Memory = styled.div`
    display:flex;
    background:trasparent;
    width:100%;
    max-height:200px;
    flex-wrap:wrap;
    flex-direction:row;
    justify-content:center;
    padding:5px;
    overflow-y:auto;
    ${Visor}
`;

/**
 * @desc Juegos grabados
 */
export const Saved = styled.div`
    display:flex;
    width:100%;
    max-height: 30px;
    align-items:center;
    ${Visor}
    padding:5px;
`;

/**
 * @desc Titulo
 */
export const Title = styled.h2`
    width:calc(100% - 20px);
    color:#111;
    font-size:18px;
    margin: 0px;
    margin-bottom:10px;
    ${ Visor }
    padding:10px;
    text-align:center
`;

/**
 * @desc Fecha del juego guardado
 */
export const DaySaved = styled.h6`
    color:#222;
    font-size:14px;
    margin:0;
    padding: 10px;
`;



/**
 * @desc Mensaje de salida
 */
export const Output = styled.output`
    display:block;
    width:100%;
    color:#222;
    font-size:14px;
    margin:0;
    padding: 10px;
    text-align:center;
`;
