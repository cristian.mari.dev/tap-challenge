/**
 *  @desc Dependencias
 */
import styled, { css } from "styled-components";

/**
 * @desc Visor
 */
const Visor = css`
    border-top-color:#878787;
    border-left-color:#878787;
    border-right-color:#fff;
    border-bottom-color:#fff;
    border-width:1px;
    border-style:solid;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
`;

/**
 * @desc Raiz
 */
export const Root = styled.div`
    display:grid;
    grid-template-columns: minmax(60px, 80px) 40px minmax(60px,80px);
    padding:10px 0px;
    ${ Visor }
    align-items:center;
    justify-content: space-around
`;

/**
 * @desc Contador
 */
export const Counter = styled.div`
    background:#010001;
    color:#de1e0a;
    padding:5px 10px;
    font-family:Calculator;
    font-size:24px;
    letter-spacing:4px;
    ${ Visor }
    text-align:center;
`;

/**
 * @desc Estado de la aplicación
 */
export const Status = styled.div`
    ${ Visor }
    font-size:20px;
    text-align:center;
    padding:5px 0px;
`;
