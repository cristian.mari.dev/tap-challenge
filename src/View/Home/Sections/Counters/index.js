/**
 * @desc Dependencias
 */
import React, { useState, useEffect } from "react";

/**
 * @desc Estilos
 */
import { Root, Counter, Status } from "./styles";

/**
 * @desc Componente de niveles
 */
export default ({ ...props }) => {

    // Indicamos la duración
    const [ duration, setDuration ] = useState( "000" );

    /**
     * @desc Intervalo de comienzo.
     */
    useEffect(() => {
        
        var interval = setInterval(() => {
            setDuration( props.game.getTimeElapsed() )
        }, 250 );

        return () => {
            clearInterval( interval );
        }
    });

    return (
        <Root>

            { /* Contador */ }
            <Counter>{ props.remainingMines }</Counter>

            { /* Estado */ }
            <Status onClick={ () => props.onReset() }>
                { !props.game.Winner && !props.game.GameOver &&  "🙂" }
                { props.game.GameOver && !props.game.Winner && "💥" }
                { !props.game.GameOver && props.game.Winner && "😎" }
            </Status>

            { /* Contador */ }
            <Counter>{ duration }</Counter>

        </Root>
    );

};