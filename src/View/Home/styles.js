/**
 *  @desc Dependencias
 */
import styled,{ css } from "styled-components";

/**
 * @desc Imagenes
 */
import MaskImage from "../../assets/img/mask.png";

/**
 * @desc Visor
 */
const Visor = css`
    border-top-color:#878787;
    border-left-color:#878787;
    border-right-color:#fff;
    border-bottom-color:#fff;
    border-width:1px;
    border-style:solid;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
`;

/**
 * @desc Raiz
 */
export const Root = styled.main`
    display:grid;
    grid-template-columns:minmax(400px, auto);
    align-items:center;
`;

/**
 * @desc Raiz
 */
export const GameBoard = styled.div`
    display:grid;
    background:#ddd;
    margin:2% auto;
    padding:10px;
    box-shadow:2px 2px 2px -1px rgba(0,0,0,0.3);
    grid-template-rows:1fr 1fr minmax(200px, 80%):
    position:relative;
    z-index:2;
`;

/**
 * @desc Mascara
 */
export const Mask = styled.div`
    display:block;
    background-image: url(${MaskImage});
    background-size:cover;
    width:100%;
    height:100%;
    position:absolute;
    top:-12.75%;
    left:0;
    filter:url(#turbulence);
`;


/**
 * @desc Boton de guardar
 */
export const SaveGame = styled.button`
    display:block;
    background:#c33;
    color:white;
    width:100%;
    padding:10px;
    ${Visor}
`;

