/**
 * @desc Dependencias
 */
const Path                          = require( 'path' );
const Webpack                       = require( 'webpack' );
const HtmlWebpackPlugin             = require( 'html-webpack-plugin' );
const Dotenv                        = require( 'dotenv' );
const CopyWebpackPlugin             = require( 'copy-webpack-plugin' );

// Importamos el .env
const EnvParsed = Dotenv.config({ "path": __dirname + '/dev.env' }).parsed;

/**
 * @desc Recopilamos los modulos.
 */
const envKeys = Object.keys( EnvParsed ).reduce( ( prev, next ) => {

  prev[`process.env.${next}`] = JSON.stringify( EnvParsed[next] );
  return prev;

}, {});

/**
 * @desc Exportamos la configuración de webpack.
 */
module.exports = {

  /**
   * @desc Observa los cambios del webpack.
   */
  watch: false,

  /**
   * @desc Mapa del codigo
   */
  devtool: false,

  /**
   * @desc Archivo principal de la aplicación.
   */
  entry: [ '@babel/polyfill', Path.resolve( __dirname, './src/root.jsx' )],

  /**
   * @desc Configuración de salida.
   */
  output: {

    /**
     * @desc Carpeta de salida
     */
    path: Path.resolve( __dirname, './build' ),

    /**
     * @desc Nombre del archivo compilado.
     */
    filename: 'assets/js/bundle.js',

    publicPath: '/' + ( process.env.NODE_ENV === 'CI' ? 'tap-challenge/' : '' )

  },

  /**
   * @desc Resolvemos las extensiones comunes
   */
  resolve: {
    extensions: [ '.js', '.jsx', '.json']
  },

  /**
   * @desc Modulos
   */
  module: {
    rules: [

      /**
       * @desc Loader: Js
       */
      {
        test: /\.m?(js|jsx)$/,
        include: Path.join(__dirname, '/src'),
        exclude: /(node_modules|bower_components)/,
        use: [{
          loader: 'babel-loader',
          options: { 
            presets: ['@babel/preset-env'],
            sourceMap: true
          }
        }]
      },

      /**
       * @desc Url Loader
       * @type's images, fonts
       */
      {
        test: /\.( png | jpg | jpeg | gif | svg | ttf | woff | eot )$/i, 
        use: 'url-loader'
      },

      /**
       * @desc File Loader
       */
      {
        test: /\.(jpe?g|png|gif|svg|)$/i,
        loader: 'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
        options: {
          name: '[name].[ext]',
          outputPath: './assets/img'
        }
      },
      
      /**
       * @desc File Loader
       */
      {
        test: /\.(ttf|eot|svg|gif|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
        options: {
          name: '[name].[ext]',
          outputPath: './assets/fonts'
        }
      },

      {
        test: /\.svg$/,
        use: [
          {
            loader: "babel-loader"
          },
          {
            loader: "react-svg-loader",
            options: {
              jsx: true 
            }
          }
        ]
      }

    ]
  },


  /**
   * @desc Plugin's
   */
  plugins: [

    new Webpack.DefinePlugin( envKeys ),

    /**
     * @desc Copiamos los lenguajes
     */
    new CopyWebpackPlugin([{
          from: Path.resolve(__dirname,'template/'),
          to: Path.resolve(__dirname,'build/')
    }]),

    /**
     * @desc Genera el index.html
     */
    new HtmlWebpackPlugin({

      /**
       * @desc Titulo
       */
      title: 'Buscaminas',

      /**
       * @desc Nombre del archivo de salida.
       */
      filename: 'index.html',

      /**
       * @desc Template
       */
      template: Path.resolve( __dirname, 'template/index.html' ),

    }),

    /**
     * @desc Actualización en vivo.
     */
    new Webpack.HotModuleReplacementPlugin()
  
  ]
};
